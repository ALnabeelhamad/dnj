# Modularising Grafana
.
This project aims to introduce a modular deployment of grafana with increased resilence. This is achieved through a hydrid terraform/ansible code.

### Requirments

This project requires the following before one can initiate the environment:

* An S3 bucket must exist. It is currently in eu-west-2 and it is called dnj-final.
* [Terraform](https://www.terraform.io/downloads.html) - Build infrastructure (aws/azure)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) - automates software provisioning, and application deployment
* [aws cli](https://docs.amazonaws.cn/en_us/cli/latest/userguide/install-linux.html) - access aws

### Getting Started

* Download this repo:
```
        https://bitbucket.org/ALnabeelhamad/dnj/src/master/
```
* cd into grafana/Management
* Provide terraform with your credentials by running :
 ```
       source mkenv.sh
```
* Initialising and applying the terraform script:
```
      terraform init
      terraform apply
```
* You will be prompted to provide a password. This will be your passowrd for accessing grafana admin unser.
* **Congratulations!** You are now able to access the grafana through:
www.academy-dnj-grafana.grads.al-labs.co.uk
This will always redirects you through a https (secured) connection

### Variable

| Name   |      Default      |  Description |
|----------|:-------------:|------:|
| region |  eu-west-2 | region you'll be building the infrustructure |
| domain |    grads.al-labs.co.uk   |   domain name  |
| key_name | dnj |   aws private key pair  |
| access_key | right-aligned | your credentials access key |
| secret_key | right-aligned |    your credentials secret key |
| image_id | ami-0274e11dced17bb5b | linux golden image |
| security_groups | academy-dnj |  name of the security group |
| vpc_cidr | 10.0.0.0/24 |   the value of your vpc cidr |
| public_subnet_cidr | 10.0.0.0/26 |    the value of your public subnet cidr |
| instance_type | t2.micro |    size of running instance |
| tag | academy-dnj-final |    name of the tag used to tag your infrusctructure |
| state_bucket | dnj-final |    name of your s3 bucket |
| user | grafana |   user's name  |
| availability_zone | eu-west-2a |    availability zone |
| ebs_name | /dev/xvdf |    name of the ebs used |
| ebs_name_provision | /dev/xvdf1 |    ebs after its been partitioned |
| allowed_ips | 77.108.144.180/32 |    the public ip that will be allowed through. The default is the AL office public ip |
| create_ebs | 1 |    Set the value to one to create an EBS, and set use_existing_ebs to 0 |
| use_existing_ebs | 0 |    Set the value to one to use an existing ebs, and set create_ebs to 0 |
| existing_ebs_id |  |    Use with use_existing_ebs to attach an existing ebs volume to the instance |

### Authors

*   **Dilyan Kostov** - infrustrcuture and provisioning grafana
*   **Nabeel Hamad** - infrustrcuture and provisioning grafana
*   **Jonathan Habte** - amazon elastic block store and resilence

### Acknowledgments

* **Steve Shilling** - unwavering commitment and support to the academy and graduates
