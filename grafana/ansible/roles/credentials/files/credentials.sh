#!/bin/bash

SecretAccessKey=$(cat /tmp/tmpCredentials | jq -r '.AccessKey.SecretAccessKey')
AccessKeyId=$(cat /tmp/tmpCredentials | jq -r '.AccessKey.AccessKeyId')


cat >/tmp/gCredentials <<_END_
[default]
aws_access_key_id = $SecretAccessKey
aws_secret_access_key = $AccessKeyId
region = eu-west-2
_END_
