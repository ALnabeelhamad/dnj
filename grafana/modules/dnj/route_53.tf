resource "aws_eip" "dnj" {
  vpc      = true
  tags {
    Name = "${var.tag}_instance_EIP"
  }
}


data "aws_route53_zone" "selected" {
 name         = "${var.domain}"
 private_zone = false
}

resource "aws_route53_record" "www" {
 depends_on = ["aws_eip_association.eip_association"]
 zone_id = "${data.aws_route53_zone.selected.zone_id}"
 name    = "www.${var.tag}-grafana.${var.domain}"
 type    = "A"
 ttl     = "60"
 records  = ["${aws_eip.dnj.public_ip}"]
}
resource "aws_route53_record" "no-www" {
 depends_on = ["aws_eip_association.eip_association"]
 zone_id = "${data.aws_route53_zone.selected.zone_id}"
 name    = "${var.tag}-grafana.${var.domain}"
 type    = "A"
 ttl     = "60"
 records  = ["${aws_eip.dnj.public_ip}"]
}
