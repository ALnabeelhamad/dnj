# VPC ID
output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

# internet gate way id
output "gateway_id" {
  value = "${aws_internet_gateway.gw.id}"
}


# security groupd id
output "security_group_id" {
  value = "${aws_security_group.ec2.id}"
}

# subnet id
output "subnet_id" {
  value = "${aws_subnet.public.id}"
}
