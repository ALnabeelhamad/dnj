resource "aws_iam_role" "grafana_role" {
  name = "${var.tag}_grafana_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "grafana_profile" {
  name = "${var.tag}_profile"
  role = "${aws_iam_role.grafana_role.name}"
}

resource "aws_iam_policy" "policy" {
  name        = "${var.tag}_policy"
  path        = "/"
  description = "${var.tag}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowReadingMetricsFromCloudWatch",
            "Effect": "Allow",
            "Action": [
                "cloudwatch:ListMetrics",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:GetMetricData"
            ],
            "Resource": "*"
        },
        {
            "Sid": "AllowReadingTagsInstancesRegionsFromEC2",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeTags",
                "ec2:DescribeInstances",
                "ec2:DescribeRegions"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "Grafana" {
  role       = "${aws_iam_role.grafana_role.name}"
  policy_arn = "${aws_iam_policy.policy.arn}"
  # policy_arn = "arn:aws:iam::109964479621:policy/GrafanaPolicy"
}
