resource "aws_instance" "dnj" {
  depends_on                  = [ "data.template_file.grafana_template" ]
  ami                         = "${var.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${var.public_subnet_id}"
  vpc_security_group_ids      = ["${aws_security_group.ec2.id}"]
  iam_instance_profile        = "${aws_iam_instance_profile.grafana_profile.name}"
  associate_public_ip_address = true
  availability_zone           = "${var.availability_zone}"
  instance_initiated_shutdown_behavior = "terminate"
  tags {
    Name = "${var.tag}"
  }

  provisioner "remote-exec" {
    inline = ["sudo yum -y install jq"]
  }
  connection {
    user         = "ec2-user"
    private_key  = "${file("~/.aws/${var.key_name}.pem")}"
    type         = "ssh"
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.grafana_template.rendered}\" > tmpCredentials"
  }
}

resource "aws_eip_association" "eip_association"{
  instance_id = "${aws_instance.dnj.id}"
  allocation_id = "${aws_eip.dnj.id}"
  depends_on = ["aws_instance.dnj","aws_eip.dnj"]
}

resource "null_resource" "ansible" {
  depends_on = ["aws_instance.dnj","aws_eip.dnj", "aws_route53_record.www","null_resource.prepdisknew", "null_resource.prepdiskexisting"]

  provisioner "local-exec" {
    command = "echo ${aws_instance.dnj.id} > ../ansible/grafana_id"
  }

  # Save the public IP for testing
  provisioner "local-exec" {
    command = "echo ${aws_eip.dnj.public_ip} > ../ansible/grafana_ip"
  }
  provisioner "local-exec" {
    command = "echo ${var.grafana_password} > ../ansible/grafana_password"
  }
  provisioner "local-exec" {
    command = "echo \"${var.tag}-grafana.${var.domain}\" > ../ansible/grafana_domain"
  }
  provisioner "local-exec" {
    command = "sleep 30"
  }
  # run ansible script
  provisioner "local-exec" {
    command = "ansible-playbook -i ../ansible/environments/dev/hosts ../ansible/site.yml"
  }
}


output "public_dns" {
  value = "${aws_instance.dnj.public_dns}"
}
