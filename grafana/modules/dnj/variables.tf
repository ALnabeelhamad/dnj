variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-2"
}

variable "image_id" {
  default = "ami-009d6802948d06e52"
}

variable "security_groups" {
}

variable "vpc_cidr" {
  default = "10.0.0.0/24"
}

variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "10.0.0.0/26"
}

variable "vpc_id" {}


variable "public_subnet_id" {}

variable "instance_type" {
  description = "size of instance used"
  default = "t2.micro"
}

variable "key_name" {
  description = "private key pair name as displayed in aws site"
  default     = "dnj"
}

variable "tag" {
  description = "associated tag"
}

variable "gateway_id" {}
variable "security_group_id" {}
variable "grafana_password" {}

variable "domain" { default = "grads.al-labs.co.uk" }

variable "availability_zone" {
  default = "eu-west-2a"
}

variable "create_ebs" {
  description = "Set to 1 to create a new EBS volume, 0 to use existing"
  default     = "1"
}

variable "use_existing_ebs" {
  description = "Set to 1 to use existing EBS Volume"
  default     = "0"
}

variable "existing_ebs_id" {
  description = "The ID of an existing EBS"
  default     = ""
}

variable "ebs_name" {}

variable "ebs_name_provision" {}
variable "allowed_ips" {
  type = "list"
}
