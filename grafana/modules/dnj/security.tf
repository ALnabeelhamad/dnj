# Create security group with web and ssh access
resource "aws_security_group" "ec2" {
  name = "${var.security_groups}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = "${var.allowed_ips}"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
#    cidr_blocks = "${var.allowed_ips}"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 3000
    to_port     = 3000
    cidr_blocks = "${var.allowed_ips}"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
#    cidr_blocks = "${var.allowed_ips}"
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id="${var.vpc_id}"
  tags {
    Name = "${var.tag}_instance_SG"
  }
}
