data "template_file" "grafana_template" {
  template = "${file("${path.module}/tempCredentials")}"

  vars {
    user            = "${aws_iam_access_key.grafana_user.user}"
    status          = "${aws_iam_access_key.grafana_user.status}"
    secret          = "${aws_iam_access_key.grafana_user.secret}"
    id              = "${aws_iam_access_key.grafana_user.id}"
  }
}

data "template_file" "ebs_name_template" {
  template = "${file("${path.module}/tempebs")}"

  vars {
    ebs_name            = "${var.ebs_name}"
    ebs_name_provision  = "${var.ebs_name_provision}"
  }
}
