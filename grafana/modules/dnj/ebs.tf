resource "aws_ebs_volume" "main" {
  availability_zone = "${var.availability_zone}"
  count                       = "${var.create_ebs}"
  size              = 40

  tags {
    Name = "/dev/xvdf"
  }
}

resource "aws_volume_attachment" "ebs_att_new" {
  count       = "${var.create_ebs}"
  device_name = "/dev/xvdf"
  volume_id   = "${aws_ebs_volume.main.id}"
  instance_id = "${aws_instance.dnj.id}"
  skip_destroy= "true"
}

resource "aws_volume_attachment" "ebs_att_old" {
  count       = "${var.use_existing_ebs}"
  device_name = "/dev/xvdf"
  volume_id   = "${var.existing_ebs_id}"
  instance_id = "${aws_instance.dnj.id}"
  skip_destroy= "true"
}

resource "null_resource" "prepdisknew" {
  count         = "${var.create_ebs}"
  depends_on    = ["aws_volume_attachment.ebs_att_new"]
  triggers = {
    template = "${data.template_file.ebs_name_template.rendered}"
  }

  # provisioner "local-exec" {
  #   command = "cat >${path.module}/mkdisk.sh <<END ${data.template_file.ebs_name_template.rendered} END"
  # }
  connection {
    user         = "ec2-user"
    private_key  = "${file("~/.aws/${var.key_name}.pem")}"
    type         = "ssh"
    host        = "${aws_eip.dnj.public_ip}"
  }

  provisioner "file" {
    # source      = "${path.module}/mkdisk.sh"
    content     = "${data.template_file.ebs_name_template.rendered}"
    destination = "/tmp/mkdisk.sh"
  }

  provisioner "remote-exec" {
    inline = [
    	"chmod +x /tmp/mkdisk.sh",
    	"sudo /tmp/mkdisk.sh"
    ]
  }
}

resource "null_resource" "prepdiskexisting" {
  count         = "${var.use_existing_ebs}"
  depends_on    = ["aws_volume_attachment.ebs_att_old"]
  triggers = {
    template = "${data.template_file.ebs_name_template.rendered}"
  }

  # provisioner "local-exec" {
  #   command = "cat >${path.module}/mkdisk.sh <<END ${data.template_file.ebs_name_template.rendered} END"
  # }
  connection {
    user         = "ec2-user"
    private_key  = "${file("~/.aws/${var.key_name}.pem")}"
    type         = "ssh"
    host        = "${aws_eip.dnj.public_ip}"
  }

  provisioner "file" {
    # source      = "${path.module}/mkdisk.sh"
    content     = "${data.template_file.ebs_name_template.rendered}"
    destination = "/tmp/mkdisk.sh"
  }

  provisioner "remote-exec" {
    inline = [
    	"chmod +x /tmp/mkdisk.sh",
    	"sudo /tmp/mkdisk.sh"
    ]
  }
}
