resource "aws_iam_user" "grafana_user" {
  name = "${var.tag}"
  path = "/system/"
  tags = {
    tag-key = "${var.tag}"
  }
}

resource "aws_iam_access_key" "grafana_user" {
  user = "${aws_iam_user.grafana_user.name}"
}

resource "aws_iam_user_policy" "grafana_user_ro" {
  name = "${var.tag}"
  user = "${aws_iam_user.grafana_user.name}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowReadingMetricsFromCloudWatch",
            "Effect": "Allow",
            "Action": [
                "cloudwatch:ListMetrics",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:GetMetricData"
            ],
            "Resource": "*"
        },
        {
            "Sid": "AllowReadingTagsInstancesRegionsFromEC2",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeTags",
                "ec2:DescribeInstances",
                "ec2:DescribeRegions"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

# ---------------------------------------------------------------------------------
