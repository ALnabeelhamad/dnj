variable "region" {}
variable "domain" {}
variable "key_name" {}
variable "access_key" {}
variable "secret_key" {}
variable "image_id"  {}
variable "security_groups"  {}
variable "vpc_cidr"  {}
variable "public_subnet_cidr"  {}
variable "instance_type"  {}
variable "tag" {}
variable "state_bucket" {}
variable "grafana_password" {}
variable "availability_zone" {}
variable "user"  {}
variable "create_ebs" {}
variable "use_existing_ebs" {}
variable "existing_ebs_id" {}
variable "ebs_name" {}
variable "ebs_name_provision" {}
variable "allowed_ips" {
  type = "list"
}
