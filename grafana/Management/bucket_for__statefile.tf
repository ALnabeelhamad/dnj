
# This is the location that we want to store the state file
terraform {
  backend "s3" {
    bucket = "dnj-final" #academy-dilyan-final
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}

# This is the location of the state file that we want to use
data "terraform_remote_state" "simplevm" {
  backend = "s3"
  config {
    bucket = "dnj-final"
    key    = "terraform.tfstate" #academy-dilyan-final
    region = "eu-west-2"
  }
}
